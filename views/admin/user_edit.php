<form action="" method="post" class="form-row text-light">
    <h3><?= $user['user_firstname'].' '.$user['user_name'] ?></h3>
    <label for="rule" class="col-12">
        <select name="rule" id="rule" class="form-control">
            <?php foreach ($roles as $k => $role):
                if ((int)$user['user_rules'] === $k): $selected = 'selected'; else: $selected = '';
                endif; ?>
            <option value="<?= $k ?>" <?= $selected ?>><?= $role ?></option>
            <?php endforeach; ?>
        </select>
    </label>
    <div class="col text-center">
        <button id="category_edit" type="submit" class="btn btn-primary">
            Modifier
        </button>
    </div>
</form>