<nav class="navbar navbar-expand-lg navbar-dark m-0">
    <a class="navbar-brand" href="<?= WEBROOT ?>">Retour à l'accueil</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <?= (($_SERVER['REQUEST_URI'] === "/books/admin")) ?
    '<div class="collapse navbar-collapse justify-content-center" id="navbar">
        <div class="btn-group list-group" role="tablist">
            <a class="btn active" href="#categories" id="category-list" data-toggle="list" role="tab">Catégories</a>
            <a class="btn" href="#articles" id="article-list" data-toggle="list" role="tab">Articles</a>
            <a class="btn" href="#utilisateurs" id="user-list" data-toggle="list" role="tab">Utilisateurs</a>
        </div>
    </div>' :
        '<div class="m-auto">
        <a class="btn btn-outline-light" href="'.WEBROOT.'admin">Retour à l\'admin</a>
        </div>'
    ?>
    <p class="m-0">PANNEAU D'ADMINISTRATION</p>
</nav>