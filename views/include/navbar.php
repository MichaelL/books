<nav class="navbar navbar-expand-lg navbar-light position-sticky">
    <a class="navbar-brand" href="<?= WEBROOT ?>">
        <?php if(isset($_SESSION['Auth']) && !empty($_SESSION['Auth']['user_picture'])): ?>
            <img style="border-radius: 50%; width: 30px; height: 30px;"
                 src="<?= WEBROOT. 'assets/images/' .$_SESSION['Auth']['user_id'].'/'.$_SESSION['Auth']['user_picture'] ?>" alt="">
        <?php else: ?>
        <img src="https://img.icons8.com/flat_round/30/000000/bookmark-book.png" alt="">
        <?php endif; ?>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
        <div class="navbar-nav">
            <?php
            $db = new PDO('mysql:host=localhost;dbname=tpphp', 'root', '');
            $categories = $db->query("SELECT * FROM categories")->fetchAll();

            foreach ($categories as $category): ?>
                <a class="nav-link" href="<?= WEBROOT ?>categories/articles/<?= $category['category_title'] ?>"
                data-color="<?= $category['category_color'] ?>">
                    <?= strtoupper(str_replace('-', ' ',$category['category_title'])) ?>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
    <?php
    if (!isset($_SESSION['Auth'])):
        if ($_SERVER['REQUEST_URI'] !== "/books/login"): ?>
            <a class="nav-link bg-light" href="<?= WEBROOT ?>login">Se connecter</a>
        <?php endif;
    else: ?>
    <div class="dropdown">
        <button style="background-color: transparent; border: 1px solid transparent;" type="button" id="profil" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php if (!empty($_SESSION['Auth']['user_rules'])): ?>
                <img src="https://img.icons8.com/bubbles/38/000000/head-profile.png" alt="profil"/>
            <?php else: ?>
                <img src="https://img.icons8.com/bubbles/38/000000/gender-neutral-user.png" alt="Élève"/>
            <?php endif; ?>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profil">
            <?php if (isset($_SESSION['Auth']['user_rules'])):
                if ($_SESSION['Auth']['user_rules'] === "1"): ?>
                    <a class="nav-link" href="<?= WEBROOT ?>admin">Administrer</a>
                <?php else: ?>
                    <a class="nav-link btn-outline-primary" href="<?= WEBROOT ?>eleves">Ma classe</a>
                <?php endif; ?>
                <a class="nav-link btn-outline-primary" href="<?= WEBROOT ?>account">Mon compte</a>
            <?php else: ?>
                <a class="nav-link btn-outline-primary" href="<?= WEBROOT ?>livres">Livres</a>
            <?php endif; ?>
            <a class="nav-link btn-outline-danger" href="<?= WEBROOT ?>logout.php">Déconnexion</a>
        </div>
    </div>
    <?php endif; ?>
</nav>