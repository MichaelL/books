<?php

class ElevesController extends Controller
{
    /**
     * Cette méthode affiche la liste des eleves
     *
     * @return void
     */
    public function index(){
        $brand = 'Ma classe';
        // On instancie le modèle "Eleves"
        $this->loadModel('Eleves');

        // On stocke la liste des catégories dans $categories
        $eleves = $this->Eleve->getAll();

        // On envoie les données à la vue index
        $this->render('index', compact('eleves',  'brand'));
    }
}