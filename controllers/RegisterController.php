<?php

class RegisterController extends Controller
{
    /**
     * @return void
     */
    public function index(){
        $brand = 'Inscription';
        if (!isset($_SESSION['Auth'])) {
            $this->loadModel('Users');

            if (isset($_POST['firstname'], $_POST['lastname'], $_POST['email'], $_POST['password'])) {

                $select = $this->Users->getUser($_POST['email']);

                if ($select) {
                    Session::setAlert('Cette <b>Adresse Email</b> est <b>déjà utilisé</b>');
                } elseif ($_POST['firstname'] === '') {
                    Session::setAlert('Veuillez indiquer votre <b>Prénom</b>');
                } elseif ($_POST['lastname'] === '') {
                    Session::setAlert('Veuillez indiquer votre <b>Nom</b>');
                } elseif ($_POST['email'] === '') {
                    Session::setAlert('Veuillez indiquer votre <b>Adresse Email</b>');
                } elseif ($_POST['password'] === '') {
                    Session::setAlert('Veuillez renseigner un <b>Mot de passe</b>');
                } elseif (strlen($_POST['password']) < 8) {
                    Session::setAlert('Le <b>Mot de passe</b> doit contenir <b>8 caractères minimum</b>');
                } else {
                    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
                    $date = date("Y-m-d");
                    $time = date("H:i:s");

                    $this->Users->create($_POST['lastname'], $_POST['firstname'], $_POST['email'], $password, $date, $time, 0);
                    Session::setAlert('Vous êtes désormais enregistré, veuillez <a href="'.WEBROOT.'login">vous connecter</a>', 'success');
                }
            }

            // On envoie les données à la vue index
            $this->render('index', compact('brand'));
        } else if ($_SESSION['Auth']['user_rules'] === "1") {
            header('Location:' . WEBROOT . 'admin');
        } else {
            header('Location:' . WEBROOT);
        }
    }
}
