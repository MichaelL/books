<h3>Catégorie <strong style="text-decoration: underline; text-decoration-color: <?= $categorie['category_color'] ?>"><?= str_replace('-', ' ', $categorie['category_title']) ?></strong></h3>

<section class="row mt-5">
    <?php foreach ($articles as $k => $article):
        ?>
        <div class="col-4 mb-3">
            <div class="card">
                <img src="<?= WEBROOT ?>assets/images/<?= $image[$k] ?>"
                     alt="<?= $article['article_title'] ?>">
                <div class="card-body">
                    <h4 class="card-title text-truncate">
                        <a href="<?= WEBROOT ?>blog/article/<?= $article['article_url'] ?>"><?= $article['article_title'] ?></a>
                    </h4>
                    <p class="card-text text-truncate"><?= $article['article_content'] ?></p>
                </div>
                <div class="card-footer text-muted text-center">
                    <small><?= $article['article_date'] ?></small>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</section>