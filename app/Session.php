<?php

class Session
{
    /**
     * @return string
     */
    public static function alert()
    {
        if (isset($_SESSION['Flash'])) {
            $type = $_SESSION['Flash']['type'];
            $message = $_SESSION['Flash']['message'];
            unset($_SESSION['Flash']);
            return "<div class='alert alert-$type'>$message</div>";
        }
        return '';
    }

    /**
     * @param string $message
     * @param string $type
     * @return void
     */
    public static function setAlert(string $message, $type = "danger")
    {
        $_SESSION['Flash']['message'] = $message;
        $_SESSION['Flash']['type'] = $type;
    }
}