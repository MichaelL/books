<?php

class Main extends Controller
{
    /**
     * @return void
     */
    public function index(){
        // On instancie les modèles "Articles" et "CategoriesController"
        $this->loadModel('Articles');

        $select = "article_id, article_title, article_content, article_picture, article_url,
        categories.category_title";

        $conditions = "INNER JOIN categories
        ON articles.article_category = categories.category_id
        ORDER BY articles.article_date
        LIMIT 3";

        // On stocke la liste des blog dans $blog
        $articles = $this->Articles->getAll($select, $conditions);

        $image = [];
        foreach ($articles as $article) {
            if ($article['article_picture'] === '') {
                $image[] = 'ND.png';
            } else {
                $image[] = $article['article_picture'];
            }
        }
        // On envoie les données à la vue index
        $this->render('index', compact('articles',  'image'));
    }
}