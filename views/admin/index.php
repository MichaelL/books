<section class="text-light">
        <div class="tab-content" id="nav-admin">
            <div class="tab-pane fade show active" id="categories" role="tabpanel" aria-labelledby="category-list">
                <?php include 'categories.php';?>
            </div>
            <div class="tab-pane fade" id="articles" role="tabpanel" aria-labelledby="article-list">
                <?php include 'articles.php';?>
            </div>
            <div class="tab-pane fade" id="utilisateurs" role="tabpanel" aria-labelledby="user-list">
                <?php include 'utilisateurs.php';?>
            </div>
        </div>
</section>