<?php

class LivresController extends Controller
{
    /**
     * Cette méthode affiche la liste les lives
     *
     * @return void
     */
    public function index(){
        $brand = 'Livres';
        // On instancie le modèle "Livres"
        $this->loadModel('Livres');

        $conditions = "WHERE dt_emprunt IS NULL";

        // On stocke la liste des catégories dans $categories
        $livres = $this->Livre->getAll('*', $conditions);

        // On envoie les données à la vue index
        $this->render('index', compact('livres',  'brand'));
    }
}