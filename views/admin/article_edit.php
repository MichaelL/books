<form action="" method="post" class="form-row text-light">
    <label for="title" class="col-12">
        Titre de l'article:
        <input id="title" name="title" type="text" class="form-control"
               value="<?= $article['article_title'] ?>">
    </label>
    <label for="image" class="col-12">
        Image à la une:
        <input id="image" name="image" type="file" class="form-control">
    </label>
    <label for="content" class="col-12">
        Contenu de l'article:
        <textarea id="content" name="content" class="form-control"
                  rows="5" style="resize: none;"><?= $article['article_content'] ?></textarea>
    </label>
    <label for="date" class="col-6">
        Date de création:
        <input id="date" name="date" type="date" class="form-control"
        value="<?= $article['article_date'] ?>">
    </label>
    <label for="category" class="col-6">
        Catégorie:
        <select name="category" id="category" class="form-control">
            <?php foreach ($categories as $categorie):
                ($article['category_title'] === $categorie['category_title'])
                    ? $selected = 'selected'
                    : $selected = ''; ?>
                <option value="<?= $categorie['category_id'] ?>"
                    <?= $selected ?>>
                    <?= $categorie['category_title'] ?>
                </option>
            <?php endforeach; ?>
        </select>
    </label>
    <label for="user" class="col-12">
        <input id="user" name="user" type="hidden" class="form-control"
               value="<?= $_SESSION['Auth']['user_id'] ?>">
    </label>
    <label for="id" class="col-12">
        <input id="id" name="id" type="hidden" class="form-control"
               value="<?= $article['article_id'] ?>">
    </label>
    <div class="col text-center">
        <button id="article_edit" type="submit" class="btn btn-primary">
            Modifier
        </button>
    </div>
</form>