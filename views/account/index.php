<form action="" method="post" enctype="multipart/form-data" class="form-row">
    <label for="firstname" class="col-12">
        Prénom:
        <input id="firstname" name="firstname" value="<?= $_SESSION['Auth']['user_firstname'] ?>"
               type="text" class="form-control">
    </label>
    <label for="lastname" class="col-12">
        Nom:
        <input id="lastname" name="lastname" value="<?= $_SESSION['Auth']['user_name'] ?>"
               type="text" class="form-control">
    </label>
    <label for="email" class="col-12">
        Email:
        <input id="email" name="email" value="<?= $_SESSION['Auth']['user_email'] ?>"
               type="email" class="form-control"
               placeholder="adresse@email.com">
    </label>
    <label for="password" class="col-12">
        Mot de passe:
        <input id="password" name="password"
               type="password" class="form-control">
    </label>
    <label for="image" class="col-12">
        Image:
        <input id="image" name="image"
               type="file" accept="image/jpeg" class="form-control"
               data-toggle="tooltip"
               data-placement="left"
               title=".jpg uniquement">
    </label>
    <div class="col text-center">
        <button id="submit" type="submit" class="btn btn-secondary">Enregistrer
        </button>
    </div>
</form>