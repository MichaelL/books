<form action="" method="post" class="form-row text-light">
    <label for="title" class="col-12">
        Titre de la catégorie:
        <input id="title" name="title" type="text" class="form-control"
               value="<?= str_replace('-', ' ', $categorie['category_title']) ?>">
    </label>
    <label for="color" class="col-6">
        Couleur associée:
        <input id="color" name="color" type="text" class="form-control"
               value="<?= $categorie['category_color'] ?>"
        style="background-color: <?= $categorie['category_color'] ?>">
    </label>
    <label for="date" class="col-6">
        Date de création:
        <input id="date" name="date" type="date" class="form-control"
               value="<?= $categorie['category_date'] ?>">
    </label>
    <label for="user" class="col-12">
        <input id="user" name="user" type="hidden" class="form-control"
               value="<?= $_SESSION['Auth']['user_id'] ?>">
    </label>
    <label for="id" class="col-12">
        <input id="id" name="id" type="hidden" class="form-control"
               value="<?= $categorie['category_id'] ?>">
    </label>
    <div class="col text-center">
        <button id="category_edit" type="submit" class="btn btn-primary">
            Modifier
        </button>
    </div>
</form>