<?php

class Categories extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "categories";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * Retourne une catégorie en fonction de son titre
     *
     * @param string $title
     * @return mixed
     */
    public function getCategorie(string $title){
        $sql = "SELECT * FROM ".$this->table." WHERE category_title=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Créer une catégorie
     *
     * @param $title
     * @param $color
     * @param $date
     * @param $user
     * @return void
     */
    public function create($title, $color, $date, $user)
    {
        $sql = "INSERT INTO ".$this->table." SET category_title=?, category_color=?, category_date=?, category_user=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $color, $date, $user]);
    }

    /**
     * Modifier une catégorie
     *
     * @param int $id
     * @param $title
     * @param $color
     * @param $date
     * @param $user
     * @return void
     */
    public function update(int $id, $title, $color, $date, $user)
    {
        $sql = "UPDATE ".$this->table." SET category_title=?, category_color=?, category_date=?, category_user=?
        WHERE category_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $color, $date, $user, $id]);
    }

    /**
     * Supprimer une catégorie
     *
     * @param int $id
     * @return void
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE category_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }

}