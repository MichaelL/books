<h3>Toutes les catégories</h3>
<section class="row mt-5">
    <?php foreach ($categories as $categorie):
        ?>
        <div class="col-3 mb-3">
            <div class="card" style="background-color: <?= $categorie['category_color'] ?>">
                <div class="card-body">
                    <h4>
                        <a class="text-dark" href="<?= WEBROOT ?>categories/articles/<?= $categorie['category_title'] ?>">
                            <?= str_replace('-', ' ', $categorie['category_title']) ?>
                        </a>
                    </h4>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</section>