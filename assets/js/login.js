const show_professeur = document.getElementById("show_professeur"),
    show_eleve = document.getElementById('show_eleve'),
    professeur = document.getElementById("professeur"),
    eleve = document.getElementById('eleve'),
    mail = document.getElementById('email'),
    pw = document.getElementById('password'),
    id_eleve = document.getElementById('id_eleve'),
    btn_professeur = document.getElementById('login_professeur'),
    btn_eleve = document.getElementById('login_eleve')

function showLogin(button, show, hidden) {
    button.addEventListener('click', function (){
        show.hidden = false;
        hidden.hidden = true;
    })
}
showLogin(show_professeur, professeur, eleve)
showLogin(show_eleve, eleve, professeur)

function disabledBtn(input, button) {
    input.addEventListener('change', function (){
        if (input.value === '') {
            button.setAttribute('disabled', 'true')
            $(button).tooltip('enable')
        } else {
            button.removeAttribute('disabled')
            $(button).tooltip('disable')
        }
    })
}
disabledBtn(pw, btn_professeur)
disabledBtn(mail, btn_professeur)
disabledBtn(id_eleve, btn_eleve)

