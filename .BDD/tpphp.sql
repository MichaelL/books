SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `tpphp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tpphp`;

CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL,
  `article_title` varchar(255) NOT NULL,
  `article_content` varchar(255) NOT NULL,
  `article_picture` varchar(255) NOT NULL,
  `article_date` date NOT NULL,
  `article_time` time NOT NULL,
  `article_flag` tinyint(1) NOT NULL,
  `article_user` int(11) NOT NULL,
  `article_category` int(11) NOT NULL,
  `article_url` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `articles` (`article_id`, `article_title`, `article_content`, `article_picture`, `article_date`, `article_time`, `article_flag`, `article_user`, `article_category`, `article_url`) VALUES
(1, 'HyperText Markup Language', 'Le HyperText Markup Language (langage de balisage d’hypertexte), généralement abrégé HTML ou dans sa dernière version HTML5, est le langage de balisage conçu pour représenter les pages web.', '', '2020-11-25', '14:22:03', 1, 1, 1, 'hypertext-markup-language'),
(6, 'Cascading Style Sheets', 'Les feuilles de style en cascade, généralement appelées CSS de l\'anglais Cascading Style Sheets, forment un langage informatique qui décrit la présentation des documents HTML.', '', '2020-12-17', '00:00:00', 0, 1, 4, 'cascading-style-sheets');

CREATE TABLE `bibli_eleve` (
  `id_eleve` int(11) NOT NULL,
  `nom_eleve` varchar(45) NOT NULL,
  `prenom_eleve` varchar(45) NOT NULL,
  `image_eleve` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `bibli_eleve` (`id_eleve`, `nom_eleve`, `prenom_eleve`, `image_eleve`) VALUES
(1, 'A', 'Aude', 'IMAGE1'),
(2, 'B', 'Béatrice', 'IMAGE2'),
(3, 'C', 'Christophe', 'IMAGE3'),
(4, 'D', 'Denis', 'IMAGE4'),
(5, 'E', 'Estelle', 'IMAGE5'),
(6, 'F', 'Florian', 'IMAGE6'),
(7, 'G', 'Gwladys', 'IMAGE7'),
(8, 'H', 'Hugo', 'IMAGE8'),
(9, 'I', 'Ingrind', 'IMAGE9'),
(10, 'J', 'Jules', 'IMAGE10'),
(11, 'K', 'Kassandra', 'IMAGE11'),
(12, 'L', 'Laurent', 'IMAGE12'),
(13, 'M', 'Maud', 'IMAGE13'),
(14, 'N', 'Nicolas', 'IMAGE14'),
(15, 'O', 'Océane', 'IMAGE15'),
(16, 'P', 'Paul', 'IMAGE16'),
(17, 'Q', 'Quentin', 'IMAGE17'),
(18, 'R', 'Romane', 'IMAGE18'),
(19, 'S', 'Sara', 'IMAGE19'),
(20, 'T', 'Thomas', 'IMAGE20'),
(21, 'U', 'Ugo', 'IMAGE21'),
(22, 'V', 'Vanessa', 'IMAGE22'),
(23, 'W', 'Wendy', 'IMAGE23'),
(24, 'X', 'Xavier', 'IMAGE24'),
(25, 'Y', 'Yona', 'IMAGE25'),
(26, 'Z', 'Zack', 'IMAGE26'),
(27, 'AA', 'Laurent', 'IMAGE27');

CREATE TABLE `bibli_emprunt` (
  `id_eleve` int(11) NOT NULL,
  `id_livre` int(11) NOT NULL,
  `dt_debut` date NOT NULL,
  `dt_prevue` date NOT NULL,
  `dt_retour` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `bibli_livre` (
  `id_livre` int(11) NOT NULL,
  `titre_livre` varchar(100) NOT NULL,
  `cd_etat` int(11) NOT NULL,
  `image_livre` varchar(45) NOT NULL,
  `dt_emprunt` date DEFAULT NULL,
  `dt_retour` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `bibli_livre` (`id_livre`, `titre_livre`, `cd_etat`, `image_livre`, `dt_emprunt`, `dt_retour`) VALUES
(1, 'LIVRE A', 1, 'LIV1', '2011-04-08', NULL),
(2, 'LIVRE B', 1, 'LIV2', NULL, NULL),
(3, 'LIVRE C', 1, 'LIV3', NULL, NULL),
(4, 'LIVRE D', 1, 'LIV4', NULL, NULL),
(5, 'LIVRE E', 1, 'LIV5', NULL, NULL),
(6, 'LIVRE F', 1, 'LIV6', NULL, NULL),
(7, 'LIVRE G', 1, 'LIV7', NULL, NULL),
(8, 'LIVRE H', 1, 'LIV8', NULL, NULL),
(9, 'LIVRE I', 1, 'LIV9', NULL, NULL),
(10, 'LIVRE J', 1, 'LIV10', NULL, NULL),
(11, 'LIVRE K', 1, 'LIV11', NULL, NULL),
(12, 'LIVRE L', 1, 'LIV12', NULL, NULL),
(13, 'LIVRE M', 1, 'LIV13', NULL, NULL),
(14, 'LIVRE N', 1, 'LIV14', NULL, NULL),
(15, 'LIVRE O', 1, 'LIV15', NULL, NULL),
(16, 'LIVRE P', 1, 'LIV16', NULL, NULL),
(17, 'LIVRE Q', 1, 'LIV17', '2011-04-08', NULL),
(18, 'LIVRE R', 1, 'LIV18', '2011-04-08', NULL),
(19, 'LIVRE S', 1, 'LIV19', '2011-04-08', NULL),
(20, 'LIVRE T', 1, 'LIV20', '2011-04-08', NULL),
(21, 'LIVRE U', 1, 'LIV21', '2011-04-08', NULL),
(22, 'LIVRE V', 1, 'LIV22', NULL, NULL),
(23, 'LIVRE W', 1, 'LIV23', NULL, NULL),
(24, 'LIVRE X', 1, 'LIV24', NULL, NULL),
(25, 'LIVRE Y', 1, 'LIV25', NULL, NULL),
(26, 'LIVRE Z', 1, 'LIV26', NULL, NULL),
(27, 'LIVRE 1', 1, 'LIV27', NULL, NULL),
(28, 'LIVRE 2', 1, 'LIV28', NULL, NULL),
(29, 'LIVRE 3', 1, 'LIV29', NULL, NULL),
(30, 'LIVRE 4', 1, 'LIV30', NULL, NULL),
(31, 'LIVRE 5', 1, 'LIV31', '2011-04-08', NULL),
(32, 'LIVRE 6', 1, 'LIV32', '2011-04-08', NULL),
(33, 'LIVRE 7', 1, 'LIV33', '2011-04-08', NULL),
(34, 'LIVRE 8', 1, 'LIV34', '2011-04-08', NULL),
(35, 'LIVRE 9', 1, 'LIV35', '2011-04-08', NULL),
(36, 'LIVRE 0', 1, 'LIV36', NULL, NULL);

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_color` varchar(255) NOT NULL,
  `category_date` date NOT NULL,
  `category_time` time NOT NULL,
  `category_flag` tinyint(1) NOT NULL,
  `category_user` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `categories` (`category_id`, `category_title`, `category_color`, `category_date`, `category_time`, `category_flag`, `category_user`) VALUES
(1, 'html', '#F16625', '2020-11-12', '14:31:46', 1, 1),
(4, 'css', '#0CF2DB', '2020-12-17', '00:00:00', 0, 1),
(5, 'php', '#0C8FF2', '2020-12-17', '00:00:00', 0, 1),
(6, 'jquery', '#F0F23D', '2020-12-17', '00:00:00', 0, 1),
(7, 'sql', '#F21E18', '2020-12-17', '00:00:00', 0, 1),
(8, 'conception-web', '#3DF279', '2020-12-17', '00:00:00', 0, 1),
(9, 'responsive-design', '#8618F2', '2020-12-17', '00:00:00', 0, 1),
(12, 'marketing-digital', '#F2A530', '2020-12-17', '00:00:00', 0, 1);

CREATE TABLE `comments` (
  `comments_id` int(11) NOT NULL,
  `comments_content` varchar(255) NOT NULL,
  `comments_date` date NOT NULL,
  `comments_time` time NOT NULL,
  `comments_flag` tinyint(1) NOT NULL,
  `comments_user` int(11) NOT NULL,
  `comments_article` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `comments` (`comments_id`, `comments_content`, `comments_date`, `comments_time`, `comments_flag`, `comments_user`, `comments_article`) VALUES
(12, 'Merci pour l\'explication !', '2020-12-16', '16:26:16', 1, 1, 1),
(18, 'J\'aime bien le HTML5 ;)', '2021-02-09', '18:40:25', 1, 16, 1),
(14, 'Le CSS c\'est ma partie préférée !', '2020-12-18', '11:39:55', 1, 16, 6);

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_firstname` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_picture` varchar(255) NOT NULL,
  `user_date` date NOT NULL,
  `user_time` time NOT NULL,
  `user_flag` tinyint(1) NOT NULL,
  `user_rules` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `users` (`user_id`, `user_name`, `user_firstname`, `user_email`, `user_password`, `user_picture`, `user_date`, `user_time`, `user_flag`, `user_rules`) VALUES
(1, 'Lemay', 'Michaël', 'michael.lemay@lilo.org', '$2y$12$vMK/2Rw/9PcLx6l5me8iU.W/MwKbRCrIPEcJS7CQKWicu/ockAYW6', 'images.jpg', '2020-11-25', '11:39:49', 1, 1),
(16, 'Dupont', 'Jean', 'jean.dupont@lilo.org', '$2y$10$PFp8eWVc219fLK8GwK52nuaRMAqI7rpkuPg7qPUW/G9b0VWKm5hu2', '', '2021-02-09', '15:06:32', 0, 0);


ALTER TABLE `articles`
  ADD PRIMARY KEY (`article_id`),
  ADD KEY `fk_article_user` (`article_user`),
  ADD KEY `fk_article_category` (`article_category`);

ALTER TABLE `bibli_eleve`
  ADD PRIMARY KEY (`id_eleve`);

ALTER TABLE `bibli_emprunt`
  ADD PRIMARY KEY (`id_eleve`,`id_livre`);

ALTER TABLE `bibli_livre`
  ADD PRIMARY KEY (`id_livre`);

ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `fk_category_user` (`category_user`);

ALTER TABLE `comments`
  ADD PRIMARY KEY (`comments_id`),
  ADD KEY `fk_comments_user` (`comments_user`),
  ADD KEY `fk_comments_article` (`comments_article`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);


ALTER TABLE `articles`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `bibli_eleve`
  MODIFY `id_eleve` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

ALTER TABLE `bibli_livre`
  MODIFY `id_livre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

ALTER TABLE `comments`
  MODIFY `comments_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
