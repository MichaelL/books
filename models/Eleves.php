<?php

class Eleves extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "bibli_eleve";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @param string $name
     * @param string $firstname
     * @return mixed
     */
    public function getEleve(string $name, string $firstname)
    {
        $sql = "SELECT * FROM ".$this->table." WHERE nom_eleve=? AND prenom_eleve=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$name, $firstname]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }
}