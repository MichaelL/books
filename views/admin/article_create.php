<form action="" method="post" class="form-row text-light">
    <label for="title" class="col-12">
        Titre de l'article:
        <input id="title" name="title" type="text" class="form-control"
               placeholder="e.g. Mon super article" required>
    </label>
    <label for="image" class="col-12">
        Image à la une:
        <input id="image" name="image" type="file" class="form-control">
    </label>
    <label for="content" class="col-12">
        Contenu de l'article:
        <textarea id="content" name="content" class="form-control"
                  rows="5" style="resize: none;" required></textarea>
    </label>
    <label for="date" class="col-6">
        Date de création:
        <input id="date" name="date" type="date" class="form-control" required>
    </label>
    <label for="category" class="col-6">
        Catégorie:
        <select name="category" id="category" class="form-control">
            <?php foreach ($categories as $categorie): ?>
                <option value="<?= $categorie['category_id'] ?>"><?= str_replace('-', ' ', $categorie['category_title']) ?></option>
            <?php endforeach; ?>
        </select>
    </label>
    <label for="user" class="col-12">
        <input id="user" name="user" type="hidden" class="form-control"
               value="<?= $_SESSION['Auth']['user_id'] ?>">
    </label>
    <div class="col text-center">
        <button id="article_create" type="submit" class="btn btn-primary">
            Créer
        </button>
    </div>
</form>