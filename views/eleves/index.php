<h2 class="p-4">Ma classe</h2>

<table class="table table-hover bg-dark text-light">
    <thead>
    <tr class="bg-warning text-dark">
        <th scope="col">#</th>
        <th scope="col" height="63px">Nom de l'élève</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($eleves as $eleve) { ?>
        <tr>
            <th scope="row"><?= $eleve['id_eleve'] ?></th>
            <td><?= $eleve['nom_eleve'].' '.$eleve['prenom_eleve'] ?></td>
        </tr>
    <?php } ?>
    <tr class="bg-warning">
        <td colspan="2" height="62px">
        </td>
    </tr>
    </tbody>
</table>