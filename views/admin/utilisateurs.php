<h2 class="p-4">Utilisateurs</h2>

<table class="table table-hover text-light">
    <thead>
    <tr class="bg-warning text-dark">
        <th scope="col">#</th>
        <th scope="col">Nom de l'utilisateur</th>
        <th scope="col" height="63px"></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($users as $user) { ?>
        <tr>
            <th scope="row"><?= $user['user_id'] ?></th>
            <td><p class="lead text-light"><?= $user['user_name'] . ' ' . $user['user_firstname'] ?></p></td>
            <td>
                <div class="text-right">
                    <a href="admin/user_edit/<?= $user['user_id'] ?>" class="btn btn-primary">EDITER</a>
                    <a href="admin/user_delete/<?= $user['user_id'] ?>" class="btn btn-danger">SUPPRIMER</a>
                </div>
            </td>
        </tr>
    <?php } ?>
    <tr class="bg-warning">
        <td colspan="3" height="62px"></td>
    </tr>
    </tbody>
</table>