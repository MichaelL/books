<html lang="fr">
<head>
    <title>Training Book <?php if (isset($brand)){ echo '| '.$brand; } ?></title>

    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="https://img.icons8.com/flat_round/16/000000/bookmark-book.png">
    <link rel="stylesheet" href="<?= WEBROOT ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= WEBROOT ?>assets/css/admin.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body class="bg-dark">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>

<?php include 'navbar.php' ?>

<div class="container my-5">
    <!-- container -->

<?= Session::alert() ?>