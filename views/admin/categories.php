<h2 class="p-4">Catégories</h2>

<table class="table table-hover text-light">
    <thead>
    <tr class="bg-warning text-dark">
        <th scope="col">#</th>
        <th scope="col">Nom de la catégorie</th>
        <th scope="col">
            <div class="text-right">
                <a href="admin/categorie_create" class="btn btn-primary">+</a>
            </div>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($categories as $categorie) { ?>
        <tr>
            <th scope="row"><?= $categorie['category_id'] ?></th>
            <td><a href="<?= WEBROOT ?>categories/articles/<?= $categorie['category_title'] ?>"
                class="btn"
                style="background-color: <?= $categorie['category_color'] ?>"
                ><?= str_replace('-', ' ', $categorie['category_title']) ?></a></td>
            <td>
                <div class="text-right">
                    <a href="admin/categorie_edit/<?= $categorie['category_title'] ?>" class="btn btn-primary">EDITER</a>
                    <a href="admin/categorie_delete/<?= $categorie['category_id'] ?>" class="btn btn-danger">SUPPRIMER</a>
                </div>
            </td>
        </tr>
    <?php } ?>
    <tr class="bg-warning">
        <td colspan="3">
            <div class="text-right">
                <a href="admin/categorie_create" class="btn btn-primary">+</a>
            </div>
        </td>
    </tr>
    </tbody>
</table>