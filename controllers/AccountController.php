<?php

class AccountController extends Controller
{
    public function index()
    {
        if (isset($_SESSION['Auth'])) {

            $brand = 'Mon Compte';

            $this->loadModel('Users');
            $user_id = $_SESSION['Auth']['user_id'];

            if (isset($_FILES['image']) && $_FILES['image']['size'] !== 0) {
                $file_name = $_FILES['image']['name'];
                $file_extension = pathinfo($file_name,PATHINFO_EXTENSION);
                // le répertoire où le fichier va être placé
                $target_dir = IMAGES.$user_id;
                // si le répertoire n'existe pas, le créer
                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, true);
                }
                // le chemin du fichier à télécharger
                $target_file = $target_dir .'/'. basename($file_name);
                // Vérifie si le fichier est une image
                $check = getimagesize($_FILES["image"]["tmp_name"]);

                if($check === false) {
                    Session::setAlert('Format non autorisé');
                }
                // Format jpg/jpeg uniquement
                elseif (!in_array($file_extension, ["jpg", "jpeg", "JPG", "JPEG"])) {
                    Session::setAlert('Format non autorisé : <b>.jpg uniquement</b>');
                }
                // Si le fichier dépasse 500 Ko
                elseif ($_FILES["image"]["size"] > 500000) {
                    Session::setAlert('Ce fichier est trop volumineux : <b>500 Ko</b> maximum autorisé');
                } else {
                    // Si le nom du fichier existe
                    if (file_exists($target_file)) {
                        unlink($target_file);
                        move_uploaded_file($_FILES["image"]["tmp_name"],$target_file);
                    } else {
                        if ($_SESSION['Auth']['user_picture'] !== '' && file_exists($target_dir.'/'.$_SESSION['Auth']['user_picture'])) {
                            unlink($target_dir . '/' . $_SESSION['Auth']['user_picture']);
                        }
                        move_uploaded_file($_FILES["image"]["tmp_name"],$target_file);
                        $this->User->updateImage((int)$user_id, $file_name);
                    }
                    Session::setAlert('<b>'.$file_name.'</b> téléchargé avec succès', 'success');
                    $_SESSION['Auth'] = $this->User->getUser($_POST['email']);
                }
            }

            if (isset($_POST['lastname'],$_POST['firstname'],$_POST['email'],$_POST['password'])) {

                if (password_verify($_POST['password'], $_SESSION['Auth']['user_password']))
                {
                    $password = $_SESSION['Auth']['user_password'];
                } else {
                    $password = password_hash( $_POST['password'],PASSWORD_BCRYPT );
                }

                if ($_POST['lastname'] !== $_SESSION['Auth']['user_name'] || $_POST['firstname'] !== $_SESSION['Auth']['user_firstname'] || $_POST['email'] !== $_SESSION['Auth']['user_email'] || $password !== $_SESSION['Auth']['user_password']){

                    $this->User->update((int)$user_id, $_POST['lastname'], $_POST['firstname'], $_POST['email'], $password, (int)$_SESSION['Auth']['user_rules']);
                    Session::setAlert('Modification enregistré', 'success');
                    $_SESSION['Auth'] = $this->User->getUser($_POST['email']);
                }
            }

            $this->render('index', compact('brand'));
        } else {
            header('Location: login');
        }
    }
}