<?php

class Comments extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "comments";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * @param $content
     * @param $date
     * @param $time
     * @param $id_user
     * @param $id_article
     * @return void
     */
    public function create($content, $date, $time, $id_user, $id_article)
    {
        $sql = "INSERT INTO ".$this->table." SET comments_content=?, comments_date=?, comments_time=?, comments_user=?, comments_article=?, comments_flag=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$content, $date, $time, $id_user, $id_article, 1]);
    }

}