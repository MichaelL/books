<h2 class="p-4">Livres disponibles</h2>
<div class="row">
    <?php foreach ($livres as $livre) { ?>
        <div class="card col-4" style="width: 18rem;">
            <img src="https://loremflickr.com/500/500/book?lock=<?= $livre['id_livre'] ?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?= $livre['titre_livre'] ?></h5>
            </div>
        </div>
    <?php } ?>
</div>
