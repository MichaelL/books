<form action="" method="post" class="form-row text-light">
    <label for="title" class="col-12">
        Titre de la catégorie:
        <input id="title" name="title" type="text" class="form-control"
               placeholder="e.g. jQuery" required>
    </label>
    <label for="color" class="col-6">
        Couleur associée:
        <input id="color" name="color" type="text" class="form-control" required>
    </label>
    <label for="date" class="col-6">
        Date de création:
        <input id="date" name="date" type="date" class="form-control" required>
    </label>
    <label for="user" class="col-12">
        <input id="user" name="user" type="hidden" class="form-control"
               value="<?= $_SESSION['Auth']['user_id'] ?>">
    </label>
    <div class="col text-center">
        <button id="category_create" type="submit" class="btn btn-primary">
            Créer
        </button>
    </div>
</form>