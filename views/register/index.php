<form action="" method="post" class="form-row">
    <label for="firstname" class="col-12">
        Prénom:
        <input id="firstname" name="firstname"
               type="text" class="form-control" required>
    </label>
    <label for="lastname" class="col-12">
        Nom:
        <input id="lastname" name="lastname"
               type="text" class="form-control" required>
    </label>
    <label for="email" class="col-12">
        Adresse Email:
        <input id="email" name="email"
               type="email" class="form-control"
               placeholder="adresse@email.com" required>
    </label>
    <label for="password" class="col-12">
        Mot de passe:
        <input id="password" name="password"
               type="password" class="form-control" required>
    </label>
    <div class="col text-center">
        <button id="register" type="submit" class="btn btn-secondary">Valider
        </button>
    </div>
</form>

<div class="col text-center">
    <a href="<?= WEBROOT ?>login">Déjà inscrit ?</a>
</div>