let navbarLink = document.getElementById("navbar"),
    navbar = document.getElementById('navbar');

const navLink = document.getElementsByClassName('nav-link'),
    link = window.location.pathname.split('/'),

    request_uri = location.pathname + location.search,

    date = new Date().getFullYear(),
    footer = document.getElementsByTagName('footer')[0],
    btn_search = document.getElementById('button-search');

btn_search.addEventListener('click', function (e) {
    const search = document.getElementById('search').value;
    if (search === '') {
        e.preventDefault();
    }
})


if(navbarLink !== null) {
    navbarLink = navbarLink.querySelectorAll(".nav-link")

    for (let i = 0; i < navbarLink.length; i++) {
        let currentNavLink = navbarLink[i].attributes['href'].value.split('/')
        $(navbarLink[i]).hover(function (){
            navbarLink[i].style.backgroundColor = navbarLink[i].attributes['data-color'].value
        }, function () {
            if (!$(navbarLink[i]).hasClass('active')) {
                navbarLink[i].removeAttribute('style')
            }
        })
        if (currentNavLink.length === 5 && currentNavLink[4] === link[4]) {
            navbarLink[i].classList.add('active')
            navbarLink[i].style.backgroundColor = navbarLink[i].attributes['data-color'].value
        }
    }
}

if (navbar !== null) {
    navbar = navbar.parentElement
}

for (let i = 0; i < navLink.length; i++) {
    let currentNavLink = navLink[i].attributes['href'].value.split('/')
    if (currentNavLink.length === 3 && currentNavLink[2] === link[2]) {
        navLink[i].classList.add('active')
    }
}

if (request_uri !== "/books/admin") {
    window.addEventListener('scroll', function (){
        if ($(window).scrollTop() > 328) {
            navbar.style.backgroundColor = 'rgba(197,198,198,1)'
        } else {
            navbar.style.backgroundColor = 'rgba(197,198,198,.8)'
        }
    })
}

if (footer) {
    footer.textContent = '\u00A9 TRAINING BOOK - ' + date;
}
