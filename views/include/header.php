<html lang="fr">
<head>
    <title>Training Book <?php if (isset($brand)){ echo '| '.$brand; } ?></title>

    <meta charset="UTF-8">
    <link rel="icon" type="image/png" href="https://img.icons8.com/flat_round/16/000000/bookmark-book.png">
    <link rel="stylesheet" href="<?= WEBROOT ?>assets/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous"></script>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<figure class="hero text-light text-center">
    <img src="https://images.unsplash.com/photo-1524995997946-a1c2e315a42f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" alt="">
    <h1 class="pt-5">Training Books</h1>
    <h2 class="small">Developpement Web</h2>
    <form action="blog" method="post" class="form-row mx-auto">
        <div class="input-group mt-5">
            <input id="search" name="search" type="text" class="form-control" placeholder="Recherche" aria-label="search" aria-describedby="button-search">
            <div class="input-group-append">
                <button class="btn btn-outline-light" type="submit" id="button-search">Ok</button>
            </div>
        </div>
    </form>
</figure>

<?php include 'navbar.php' ?>

<div class="container my-5">
    <!-- container -->

<?= Session::alert() ?>