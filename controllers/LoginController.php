<?php

class LoginController extends Controller
{
    /**
     * @return void
     */
    public function index(){
        $brand = 'Connexion';
        if (!isset($_SESSION['Auth'])) {

            if (isset($_POST['id_eleve'])) {

                $this->loadModel('Eleves');
                $nom = $_POST['id_eleve'][0];
                $prenom = substr($_POST['id_eleve'], 1);
                $select = $this->Eleve->getEleve($nom, $prenom);

                if ($select) {
                    $_SESSION['Auth'] = $select;
                    Session::setAlert('Vous êtes désormais connecté', 'success');
                    header('Location:' . WEBROOT . 'livres');
                    die();
                }
            }

            if (isset($_POST['email'], $_POST['password'])) {

                $this->loadModel('Users');
                $select = $this->Users->getUser($_POST['email']);

                if ($select) {
                    $_SESSION['Auth'] = $select;
                    if (password_verify($_POST['password'], $_SESSION['Auth']['user_password'])) {

                        Session::setAlert('Vous êtes désormais connecté', 'success');
                        if ($_SESSION['Auth']['user_rules'] === "1") {
                            header('Location:' . WEBROOT . 'admin');
                        } else {
                            header('Location:' . WEBROOT);
                        }
                        die();
                    }

                    Session::setAlert('<b>Mot de passe</b> incorrect');
                } else {
                    Session::setAlert('<b>Email</b> incorrect');
                }
            }

            // On envoie les données à la vue index
            $this->render('index', compact('brand'));
        } else if ($_SESSION['Auth']['user_rules'] === "1") {
            header('Location:' . WEBROOT . 'admin');
        } else {
            header('Location:' . WEBROOT);
        }
    }
}