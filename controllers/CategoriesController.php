<?php

class CategoriesController extends Controller
{
    /**
     * Cette méthode affiche la liste des categories
     *
     * @return void
     */
    public function index(){
        $brand = 'Catégorie';
        // On instancie le modèle "Categories"
        $this->loadModel('Categories');

        // On stocke la liste des catégories dans $categories
        $categories = $this->Categories->getAll();

        // On envoie les données à la vue index
        $this->render('index', compact('categories',  'brand'));
    }

    /**
     * Méthode permettant d'afficher les articles à partir de la catégorie
     *
     * @param string $url
     * @return void
     * @throws Exception
     */
    public function articles(string $url){
        if ($url) {
            // On instancie les modèles "Categories" et "Articles"
            $this->loadModel('Categories');
            $this->loadModel('Articles');

            $categories = $this->Categories->getCategorie($url);

            $conditions = "WHERE article_category=" . $categorie['category_id'];

            $articles = $this->Articles->getAll("*", $conditions);

            $image = [];
            foreach ($articles as $article) {
                if ($article['article_picture'] === '') {
                    $image[] = 'ND.png';
                } else {
                    $image[] = $article['article_picture'];
                }
            }
            // On envoie les données à la vue index
            $this->render('articles', compact('articles', 'image', 'categories'));
        } else {
            header('Location:' . WEBROOT);
        }
    }
}