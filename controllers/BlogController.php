<?php

class BlogController extends Controller{
    /**
     * Cette méthode affiche la liste des articles
     *
     * @return void
     */
    public function index(){
        $brand = 'Blog';
        // On instancie le modèle "Articles"
        $this->loadModel('Articles');

        $select = "article_id, article_title, article_content, article_picture, article_date, article_url,
        category.category_title";

        $conditions = "INNER JOIN categories
        ON articles.article_category = categories.category_id";

        if (isset($_POST['search'])) {
            $conditions .= " WHERE article.article_content LIKE '%".$_POST['search']."%' 
            OR article.article_title LIKE '%".$_POST['search']."%' ";
        }

        // On stocke la liste des articles dans $articles
        $articles = $this->Article->getAll($select, $conditions);

        $image = [];
        foreach ($articles as $article) {
            if ($article['article_picture'] === '') {
                $image[] = 'ND.png';
            } else {
                $image[] = $article['article_picture'];
            }
        }
        // On envoie les données à la vue index
        $this->render('index', compact('articles', 'image', 'brand'));
    }

    /**
     * Méthode permettant d'afficher un article à partir de son url
     *
     * @param string $url
     * @return void
     * @throws Exception
     */
    public function article(string $url){
        if ($url) {
            // On instancie le modèle "Articles"
            $this->loadModel('Articles');
            $this->loadModel('Comments');

            // On stocke l'article dans $article
            $article = $this->Article->getArticle($url);

            if ($article) {
                $date = new DateTime($article['article_date']);
                $article['article_date'] = $date->format('d/m/Y');

                $select = "comments_content, comments_date, comments_time,
                user.user_name, user.user_firstname, user.user_picture, user.user_id";

                $conditions = "LEFT JOIN user
                ON user.user_id = comments.comments_user
                WHERE comments_article=" . $article['article_id'].
                " ORDER BY comments_date DESC, comments_time DESC";

                $comments = $this->Comments->getAll($select, $conditions);

                foreach ($comments as $k => $comment) {
                    $date = new DateTime($comment['comments_date']);
                    $comments[$k]['comments_date'] = $date->format('d/m/Y');

                    if ($comment['user_picture'] === '') {
                        $user_image[] = 'ND.png';
                    } else {
                        $user_image[] = $comment['user_id'].'/'.$comment['user_picture'];
                    }
                }

                if ($article['article_picture'] === '') {
                    $image = 'ND.png';
                } else {
                    $image = $article['article_picture'];
                }

                if (isset($_POST['comment']) && $_POST['comment'] !== "") {
                    $date = new DateTime();
                    $time = $date->format('H:i:s');
                    $date = $date->format('Y-m-d');
                    $this->Comments->create($_POST['comment'], $date, $time, $_SESSION['Auth']['user_id'], $article['article_id']);
                    Session::setAlert("Votre commentaire est en cours de traitement", "success");
                }

                // On envoie les données à la vue article
                $this->render('article', compact('article', 'image', 'comments', 'user_image'));
            } else {
                header('Location:' . WEBROOT);
                Session::setAlert("<b class='lead font-weight-bold'>404 :</b> L'article recherché n'existe pas");
            }
        } else {
            header('Location:' . WEBROOT);
        }
    }
}