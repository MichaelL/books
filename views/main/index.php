<section>
    <h2> Bienvenue au CESI <?php if (isset($_SESSION['Auth'])) :
            if (!empty($_SESSION['Auth']['user_firstname'])):
                echo $_SESSION['Auth']['user_firstname'];
            elseif (!empty($_SESSION['Auth']['prenom_eleve'])):
                echo $_SESSION['Auth']['prenom_eleve'];
            endif;
        endif; ?></h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab cumque debitis delectus deleniti dolore eum
        fugiat harum, impedit inventore, iure quos reiciendis unde vero voluptatem voluptatum? Exercitationem
        inventore natus suscipit.</p>
</section>

<section class="row mt-5">
        <?php
        foreach ($articles as $k => $article):
            ?>
            <div class="col-4 mb-3">
                <div class="card">
                    <span class="badge badge-dark"><?= $article['category_title'] ?></span>
                    <img src="<?= WEBROOT ?>assets/images/<?= $image[$k] ?>"
                         alt="<?= $article['article_title'] ?>">
                    <div class="card-body">
                        <h4 class="card-title text-truncate">
                            <a href="<?= WEBROOT ?>blog/article/<?= $article['article_url'] ?>"><?= $article['article_title'] ?></a>
                        </h4>
                        <p class="card-text text-truncate"><?= $article['article_content'] ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
</section>
