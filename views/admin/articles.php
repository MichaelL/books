<h2 class="p-4">Articles</h2>

<table class="table table-hover text-light">
    <thead>
    <tr class="bg-warning text-dark">
        <th scope="col">#</th>
        <th scope="col">Nom de l'article</th>
        <th scope="col">
            <div class="text-right">
                <a href="admin/article_create" class="btn btn-primary">+</a>
            </div>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($articles as $article) { ?>
        <tr>
            <th scope="row"><?= $article['article_id'] ?></th>
            <td><a href="<?= WEBROOT ?>blog/article/<?= $article['article_url'] ?>"
                   class="lead text-light"><?= $article['article_title'] ?></a></td>
            <td>
                <div class="text-right">
                    <a href="admin/article_edit/<?= $article['article_url'] ?>" class="btn btn-primary">EDITER</a>
                    <a href="admin/article_delete/<?= $article['article_id'] ?>" class="btn btn-danger">SUPPRIMER</a>
                </div>
            </td>
        </tr>
    <?php } ?>
    <tr class="bg-warning">
        <td colspan="3">
            <div class="text-right">
                <a href="admin/article_create" class="btn btn-primary">+</a>
            </div>
        </td>
    </tr>
    </tbody>
</table>