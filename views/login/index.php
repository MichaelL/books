<div class="row">
    <a id="show_professeur" class="col-6 text-center" style="cursor: pointer; text-decoration: none;">
        <div class="jumbotron">
            <img src="https://img.icons8.com/bubbles/50/000000/head-profile.png" alt="Professeur"/>
            Je suis professeur
        </div>
    </a>
    <a id="show_eleve" class="col-6 text-center" style="cursor: pointer; text-decoration: none;">
        <div class="jumbotron">
            <img src="https://img.icons8.com/bubbles/50/000000/gender-neutral-user.png" alt="Élève"/>
            Je suis un eleve
        </div>
    </a>
</div>

<div id="professeur" hidden>
    <form action="" method="post" class="form-row">
        <label for="email" class="col-12">
            Adresse Email:
            <input id="email" name="email" type="email" class="form-control"
                   placeholder="adresse@email.com" required>
        </label>
        <label for="password" class="col-12">
            Mot de passe:
            <input id="password" name="password" type="password" class="form-control" required>
        </label>
        <div class="col text-center">
            <button id="login_professeur" type="submit" class="btn btn-secondary"
                    data-toggle="tooltip"
                    data-placement="right"
                    title="Entrée vos adresse mail et mot de passe"
                    disabled>Se connecter
            </button>
        </div>
    </form>

    <div class="col text-center">
        <a href="<?= WEBROOT ?>register">Pas encore inscrit ?</a>
    </div>
</div>

<div id="eleve" hidden>
    <form action="" method="post" class="form-row">
        <label for="id_eleve" class="col-12">
            Identifiant:
            <input id="id_eleve" name="id_eleve" type="text" class="form-control" required>
        </label>
        <div class="col text-center">
            <button id="login_eleve" type="submit" class="btn btn-secondary" disabled>Se connecter
            </button>
        </div>
    </form>
</div>

<script type="application/javascript" src="<?= WEBROOT ?>assets/js/login.js"></script>