<?php

class Users extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "users";

        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * Retourne un utilisateur en fonction de son email
     *
     * @param string $mail
     * @return mixed
     */
    public function getUser(string $mail){
        $sql = "SELECT * FROM ".$this->table." WHERE user_email=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$mail]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Créer un utilisateur
     *
     * @param $name
     * @param $firstname
     * @param $email
     * @param $password
     * @param $date
     * @param $time
     * @param int $rules
     * @return void
     */
    public function create($name, $firstname, $email, $password, $date, $time, int $rules)
    {
        $sql = "INSERT INTO ".$this->table." SET user_name=?, user_firstname=?, user_email=?, user_password=?, user_date=?, user_time=?, user_rules=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$name, $firstname, $email, $password, $date, $time, $rules]);
    }

    /**
     * Modifier un utilisateur
     *
     * @param int $id
     * @param $name
     * @param $firstname
     * @param $email
     * @param $password
     * @param int $rules
     * @return void
     */
    public function update(int $id, $name, $firstname, $email, $password, int $rules)
    {
        $sql = "UPDATE ".$this->table." SET user_name=?, user_firstname=?, user_email=?, user_password=?, user_rules=?
        WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$name, $firstname, $email, $password, $rules, $id]);
    }

    /**
     * Mis à jour de l'image
     *
     * @param int $id
     * @param $picture
     * @return void
     */
    public function updateImage(int $id, $picture)
    {
        $sql = "UPDATE ".$this->table." SET user_picture=?
        WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$picture, $id]);
    }

    /**
     * Supprimer un utilisateur
     *
     * @param int $id
     * @return void
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE user_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }

}