<article>
    <div class="row mb-5">
        <div class="col-auto">
            <span class="badge badge-dark position-absolute"><?= $article['category_title'] ?></span>
            <img src="<?= WEBROOT ?>assets/images/<?= $image ?>"
                 alt="<?= $article['article_title'] ?>">
        </div>
        <div class="col-auto">
            <h2 class="text-primary"><?= $article['article_title'] ?></h2>
            <p class="d-flex justify-content-between"><?= $article['user_name'] . ' ' . $article['user_firstname'] ?>
                <small class="text-muted"><?= $article['article_date'] ?></small></p>

        </div>
    </div>
    <p><?= $article['article_content'] ?></p>


    <section class="mt-5">
        <h3>Commentaires</h3>
        <?php if (isset($_SESSION['Auth']['user_id'])):?>
            <form action="" method="post" class="form-row">
                <div class="input-group mb-3">
                    <label for="comment"></label>
                    <input id="comment" name="comment" type="text" class="form-control"
                    placeholder="Ajouter un commentaire">
                    <div class="input-group-append">
                        <button id="submit" type="submit" class="btn btn-primary">Commenter</button>
                    </div>
                </div>
            </form>
        <?php endif; ?>
        <?php foreach ($comments as $k => $comment):?>
        <div class="list-group-item">
            <article class="row">
                <div class="col-auto">
                    <img style="border-radius: 50%; width: 30px; height: 30px;"
                         src="<?= WEBROOT. 'assets/images/' .$user_image[$k] ?>" alt="">
                </div>
                <div class="col">
                    <p class="d-flex justify-content-between m-0"><?= $comment['user_name'] ?> <?= $comment['user_firstname'] ?>
                        <small class="mb-3 text-muted">Le <?= $comment['comments_date'] ?> à <?= $comment['comments_time'] ?></small>
                    </p>
                </div>
                <div class="col-12">
                    <hr class="m-2">
                    <p class="m-0"><?= $comment['comments_content'] ?></p>
                </div>
            </article>
        </div>
        <?php endforeach; ?>
    </section>
</article>