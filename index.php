<?php
session_start();

// On génère une constante contenant le chemin vers la racine publique du projet
define('ROOT', str_replace('index.php','',$_SERVER['SCRIPT_FILENAME']));
define('DIRECTORY', dirname(__FILE__, 2));
define('WEBROOT', "/books/");
define('IMAGES', DIRECTORY.WEBROOT.'assets/images/');

if (false !== strpos($_SERVER['REQUEST_URI'], "/books/admin")) {
    (isset($_SESSION['Auth']) && $_SESSION['Auth']['user_rules'] === "1") ?: header('Location: '.WEBROOT.'login');
}

require_once(ROOT.'app/Session.php');

// On appelle le modèle et le contrôleur principaux
require_once(ROOT.'app/Model.php');
require_once(ROOT.'app/Controller.php');

// On sépare les paramètres et on les met dans le tableau $params
$params = explode('/', $_GET['p']);

// Si au moins 1 paramètre existe
if($params[0] !== ""){
    // On sauvegarde le 1er paramètre dans $controller en mettant sa 1ère lettre en majuscule
    $controller = ucfirst($params[0]).'Controller';

    // On sauvegarde le 2ème paramètre dans $action si il existe, sinon index
    $action = isset($params[1]) ? $params[1] : 'index';


    if (file_exists(ROOT.'controllers/'.$controller.'.php')) {    // On appelle le contrôleur
        require_once(ROOT . 'controllers/' . $controller . '.php');

        // On instancie le contrôleur
        $controller = new $controller();
    } else {
        header('Location:' . WEBROOT);
        Session::setAlert("<b class='lead font-weight-bold'>404 :</b> La page recherchée n'existe pas");
    }

    if(method_exists($controller, $action)){
        if (count($params) > 1 && $_SERVER['REDIRECT_URL'] === '/books/'.$params[0].'/'.$params[1]) {
            header('Location:' . WEBROOT);
        }else{
            unset($params[0], $params[1]);
            call_user_func_array([$controller, $action], $params);
        }
    }else{
        // On envoie le code réponse 404
        header('Location:' . WEBROOT);
        Session::setAlert("<b class='lead font-weight-bold'>404 :</b> La page recherchée n'existe pas");
    }
}else{
    // Ici aucun paramètre n'est défini
    // On appelle le contrôleur par défaut
    require_once(ROOT.'controllers/Main.php');

    // On instancie le contrôleur
    $controller = new Main();

    // On appelle la méthode index
    $controller->index();
}
