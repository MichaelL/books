<?php
(false !== strpos($_SERVER['REQUEST_URI'], "/books/admin"))
    ? include 'views/admin/include/header.php'
    : include 'views/include/header.php';
?>

    <main>
        <?= $content ?>
    </main>

<?php include 'views/include/footer.php'; ?>