<?php

class Articles extends Model
{

    public function __construct()
    {
        // Nous définissons la table par défaut de ce modèle
        $this->table = "articles";
    
        // Nous ouvrons la connexion à la base de données
        $this->getConnection();
    }

    /**
     * Retourne un article en fonction de son url
     *
     * @param string $url
     * @return mixed
     */
    public function getArticle(string $url){

        $select = "article_id, article_title, article_content, article_picture, article_date, article_url,
        categories.category_title, users.user_name, users.user_firstname";

        $conditions = "INNER JOIN categories
        ON articles.article_category = categories.category_id
        JOIN users
        ON users.user_id = articles.article_user";

        $sql = "SELECT ".$select." FROM ".$this->table." ".$conditions." WHERE article_url=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$url]);
        return $query->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Créer un article
     *
     * @param $title
     * @param $content
     * @param $picture
     * @param $date
     * @param $user
     * @param int $category
     * @param $url
     * @return void
     */
    public function create($title, $content, $picture, $date, $user, int $category, $url)
    {
        $sql = "INSERT INTO ".$this->table." SET article_title=?, article_content=?, article_picture=?, article_date=?, article_user=?, article_category=?, article_url=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $content, $picture, $date, $user, $category, $url]);
    }

    /**
     * Modifier un article
     *
     * @param int $id
     * @param $title
     * @param $content
     * @param $picture
     * @param $date
     * @param int $category
     * @param $url
     * @return void
     */
    public function update(int $id, $title, $content, $picture, $date, int $category, $url)
    {
        $sql = "UPDATE ".$this->table." SET article_title=?, article_content=?, article_picture=?, article_date=?, article_category=?, article_url=?
        WHERE article_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$title, $content, $picture, $date, $category, $url, $id]);
    }

    /**
     * Supprimer un article
     *
     * @param int $id
     * @return void
     */
    public function delete(int $id)
    {
        $sql = "DELETE FROM ".$this->table." WHERE article_id=?";
        $query = $this->_connexion->prepare($sql);
        $query->execute([$id]);
    }

}