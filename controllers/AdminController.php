<?php

class AdminController extends Controller
{
    /**
     * @return void
     */
    public function index(){
        $brand = 'Admin';

        // On instancie les modèles "Articles", "Categories" et "Users"
        $this->loadModel('Articles');
        $this->loadModel('Categories');
        $this->loadModel('Users');

        // On stocke la liste des blog dans $blog
        $articles = $this->Articles->getAll();
        // la liste des categories dans $categories
        $categories = $this->Categories->getAll();
        // et la liste des utilisateurs dans $users
        $users = $this->Users->getAll();

        // On envoie les données à la vue index
        $this->render('index', compact('articles', 'categories', 'users', 'brand'));
    }

    /**
     * @param string $title
     * @return void
     */
    public function categorie_edit(string $title)
    {
        $brand = 'Catégorie Édition';

        $this->loadModel('Categories');

        $categorie = $this->Categories->getCategorie($title);

        if (isset($_POST['title'], $_POST['color'], $_POST['date'])) {
            if (!preg_match('/^[A-Za-z -]+$/', $_POST['title'])) {
                Session::setAlert('Format non autorisé');
            } else {
                    $this->Categories->update($_POST['id'], strtolower(str_replace(' ', '-', $_POST['title'])), $_POST['color'], $_POST['date'], $_POST['user']);

                    Session::setAlert('Catégorie modifiée', 'success');
                    header('Location:' . WEBROOT . 'admin');
            }
        }
        $this->render('categorie_edit', compact('categorie'));
    }

    /**
     * @return void
     */
    public function categorie_create()
    {
        $brand = 'Catégorie Création';

        $this->loadModel('Categories');

        if (isset($_POST['title'], $_POST['color'], $_POST['date'])) {
            $url = strtolower(str_replace(' ', '-', $_POST['title']));
            if (!preg_match('/^[A-Za-z -]+$/', $_POST['title'])) {
                Session::setAlert('Format non autorisé');
            } else {
                $title = $this->Categories->getCategorie($url);

                if (!$title) {
                    $this->Categories->create($title, $_POST['color'], $_POST['date'], $_POST['user']);

                    Session::setAlert('Catégorie créée', 'success');
                    header('Location:' . WEBROOT . 'admin');
                } else {
                    Session::setAlert('Cette catégorie existe déjà');
                }
            }
        }
        $this->render('categorie_create');
    }

    /**
     * @param integer $id
     * @return void
     */
    public function categorie_delete(int $id)
    {
        $this->loadModel('Categories');

        $this->Categories->delete($id);

        Session::setAlert('Catégorie supprimée', 'success');
        header('Location:' . WEBROOT . 'admin');
    }

    /**
     * @param string $url
     * @return void
     */
    public function article_edit(string $url)
    {
        $brand = 'Articles Édition';

        $this->loadModel('Articles');
        $this->loadModel('Categories');

        $categories = $this->Categories->getAll();

        $article = $this->Articles->getArticle($url);

        if (isset($_POST['title'], $_POST['image'], $_POST['content'], $_POST['date'])) {

            if (!preg_match('/^[A-Za-z -]+$/', $_POST['title'])) {
                Session::setAlert('Format non autorisé');
            } else {
                $this->Articles->update($_POST['id'], $_POST['title'], $_POST['content'], $_POST['image'], $_POST['date'], $_POST['category'], strtolower(str_replace(' ', '-', $_POST['title'])));

                Session::setAlert('Articles modifiée', 'success');
                header('Location:' . WEBROOT . 'admin');
            }
        }
        $this->render('article_edit', compact('categories', 'article', 'brand'));
    }

    /**
     * @return void
     */
    public function article_create()
    {
        $brand = 'Articles Création';

        $this->loadModel('Articles');
        $this->loadModel('Categories');

        $categories = $this->Categories->getAll();

        if (isset($_POST['title'], $_POST['image'], $_POST['content'], $_POST['date'])) {

            $url = strtolower(str_replace(' ', '-', $_POST['title']));

            if (!preg_match('/^[A-Za-z -]+$/', $_POST['title'])) {
                Session::setAlert('Format non autorisé');
            } else {
                $title = $this->Articles->getArticle($url);

                if (!$title) {
                    $this->Articles->create($_POST['title'], $_POST['content'], $_POST['image'], $_POST['date'], $_POST['user'], $_POST['category'], $url);

                    Session::setAlert('Articles créé', 'success');
                    header('Location:' . WEBROOT . 'admin');
                } else {
                    Session::setAlert('Cet article existe déjà');
                }
            }
        }
        $this->render('article_create', compact('categories'));
    }

    /**
     * @param integer $id
     * @return void
     */
    public function article_delete(int $id)
    {
        $this->loadModel('Articles');

        $this->Articles->delete($id);

        Session::setAlert('Articles supprimé', 'success');
        header('Location:' . WEBROOT . 'admin');
    }

    public function user_edit(int $id)
    {
        $brand = 'Utilisateur Édition';

        $this->loadModel('Users');

        $user = $this->Users->getById('user_id', $id);

        $roles = [1 => "Administrateur", 0 => "Membre"];

        if (isset($_POST['rule'])) {
            $this->Users->update((int)$user['user_id'], $user['user_name'], $user['user_firstname'], $user['user_email'], $user['user_password'], (int)$_POST['rule']);
                Session::setAlert('Role utilisateur modifié', 'success');
                header('Location:' . WEBROOT . 'admin');
        }
        $this->render('user_edit', compact('user', 'roles', 'brand'));
    }

    /**
     * @param integer $id
     * @return void
     */
    public function user_delete(int $id)
    {
        $this->loadModel('Users');

        $this->Users->delete($id);

        Session::setAlert('Utilisateur supprimé', 'success');
        header('Location:' . WEBROOT . 'admin');
    }
}